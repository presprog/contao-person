<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * @package Person
 * @author mindbird
 * @license GNU/LGPL
 * @copyright mindbird 2013
 */

/**
 * Table person_entry
 */
$GLOBALS ['TL_DCA'] ['tl_person'] = array(

    // Config
    'config' => array(
        'dataContainer' => 'Table',
        'ptable' => 'tl_person_archive',
        'switchToEdit' => true,
        'enableVersioning' => true,
        'sql' => array(
            'keys' => array(
                'id' => 'primary',
                'pid' => 'index'
            )
        )
    ),
    // List
    'list' => array(
        'sorting' => array(
            'mode' => 4,
            'fields' => array(
                'sorting'
            ),
            'headerFields' => array(
                'title'
            ),
            'child_record_callback' => array(
                'tl_person',
                'listPerson'
            )
        ),
        'label' => array(
            'fields' => array(
                'lastname',
                'firstname'
            ),
            'format' => '%s, %s'
        ),
        'global_operations' => array(
            'all' => array(
                'label' => &$GLOBALS ['TL_LANG'] ['MSC'] ['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"'
            )
        ),
        'operations' => array(
            'edit' => array(
                'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'copy' => array(
                'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif'
            ),
            'delete' => array(
                'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS ['TL_LANG'] ['MSC'] ['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'toggle' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_person']['toggle'],
                'icon'                => 'visible.gif',
                'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'     => array('tl_person', 'toggleIcon')
            ),
            'show' => array(
                'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['show'],
                'href' => 'act=show',
                'icon' => 'show.gif'
            )
        )
    ),
    // Palettes
    'palettes' => array(
        'default' => '{name_legend},firstname, lastname, function; {address_legend}, street, street_number, postal_code, city; {contact_legend}, phone, email; {image_legend}, image; {description_legend:hide}, description; {publish_legend}, published;'
    ),
    // Fields
    'fields' => array(
        'id' => array(
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array(
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array(
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'sorting' => array(
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'firstname' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['firstname'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'tl_class' => 'w50',
                'maxlength' => 255
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'lastname' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['lastname'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'tl_class' => 'w50',
                'maxlength' => 255
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'function' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['function'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'maxlength' => 255
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'street' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['street'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'maxlength' => 255
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'street_number' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['street_number'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'maxlength' => 255
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'postal_code' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['postal_code'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'maxlength' => 5,
                'rgxp' => 'digit'
            ),
            'sql' => "varchar(5) NOT NULL default ''"
        ),
        'city' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['city'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'maxlength' => 255
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'phone' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['phone'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'maxlength' => 255,
                'rgxp' => 'phone'
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'email' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['email'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'maxlength' => 255,
                'rgxp' => 'email'
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'image' => array(
            'label' => &$GLOBALS ['TL_LANG'] ['tl_person'] ['image'],
            'exclude' => true,
            'search' => false,
            'inputType' => 'fileTree',
            'eval' => array(
                'filesOnly' => true,
                'fieldType' => 'radio',
                'tl_class' => 'clr',
                'extensions' => 'jpg, jpeg, png, gif'
            ),
            'sql' => "binary(16) NULL"
        ),
        'description' => array(
			'label'                   => &$GLOBALS['TL_LANG']['tl_person']['description'],
			'exclude'                 => true,
			'inputType'               => 'textarea',
			'search'                  => true,
			'eval'                    => array('rte'=>'tinyMCE'),
			'sql'                     => "text NULL"
		),
        'published' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_person']['published'],
            'exclude' => true,
            'default' => 1,
            'inputType' => 'checkbox',
            'eval' => array('doNotCopy' => true),
            'sql' => "char(1) NOT NULL default ''"
        ),
    )
);

class tl_person extends Backend
{
    public function listPerson($row)
    {
        if ($row['image'] != null) {
            $objFile = \FilesModel::findByPk(deserialize($row['image']));
            $singleSRC = $objFile->path;
            $sReturn = '<figure style="float: left; margin-right: 1em;"><img src="' . Image::get($singleSRC, 80, 80,
                    'center_top') . '"></figure>';
        }
        $sReturn .= '<div>' . $row ['lastname'] . ', ' . $row ['firstname'] . '</div>';

        return $sReturn;
    }

    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid'))) {
    		$this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
    		$this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!\BackendUser::getInstance()->hasAccess('tl_person::published', 'alexf')) {
    		return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published']) {
    		$icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"').'</a> ';
    }
     public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
  	{
  		// Set the ID and action
  		Input::setGet('id', $intId);
  		Input::setGet('act', 'toggle');

  		if ($dc) {
  			$dc->id = $intId; // see #8043
  		}

  		// Check the field access
  		if (!\BackendUser::getInstance()->hasAccess('tl_person::published', 'alexf')) {
  			$this->log('Not enough permissions to publish/unpublish person ID "'.$intId.'"', __METHOD__, TL_ERROR);
  			$this->redirect('contao/main.php?act=error');
  		}

  		$objVersions = new Versions('tl_person', $intId);
  		$objVersions->initialize();

  		// Trigger the save_callback
  		if (is_array($GLOBALS['TL_DCA']['tl_person']['fields']['published']['save_callback'])) {
  			foreach ($GLOBALS['TL_DCA']['tl_person']['fields']['published']['save_callback'] as $callback)
  			{
  				if (is_array($callback)) {
  					$this->import($callback[0]);
  					$blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
  				} elseif (is_callable($callback)) {
  					$blnVisible = $callback($blnVisible, ($dc ?: $this));
  				}
  			}
  		}

  		// Update the database
  		$this->Database->prepare("UPDATE tl_person SET tstamp=". time() .", published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
  					   ->execute($intId);

  		$objVersions->create();
  	}
}

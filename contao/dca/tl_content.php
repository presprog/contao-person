<?php
$GLOBALS['TL_DCA']['tl_content']['palettes']['person'] = '{type_legend},type,headline;{person_legend},personID,size;{template_legend},personTpl,customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space;{invisible_legend:hide},invisible,start,stop';

$GLOBALS ['TL_DCA'] ['tl_content'] ['fields'] ['personID'] = array(
    'label' => &$GLOBALS ['TL_LANG'] ['tl_content'] ['personID'],
    'default' => '',
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_content_person', 'getPersonList'),
    'eval' => array(
        'mandatory' => true,
        'chosen' => true
    ),
    'sql' => "varchar(10) NOT NULL default ''"
);

$GLOBALS ['TL_DCA'] ['tl_content'] ['fields']['personTpl'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_content']['personTpl'],
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_content_person', 'getPersonTemplates'),
    'eval' => array('includeBlankOption' => true, 'chosen' => true, 'tl_class' => 'w50'),
    'sql' => "varchar(64) NOT NULL default ''"
);

class tl_content_person extends \Backend
{
    public function getPersonTemplates()
    {
        return $this->getTemplateGroup('ce_person');
    }

    public function getPersonList() {

        $objPersons = \Contao\PersonModel::findAll();
        $objArchives = \Contao\PersonArchiveModel::findAll();
        $invertedName = true; // lastname, firstname

        $return = array();

        foreach($objArchives as $objArchive) {

            $persons = array();

            foreach($objPersons as $objPerson) {
                if ($objPerson->pid === $objArchive->id) {
                    $persons[(string) $objPerson->id] = $objPerson->getFullName($invertedName);
                }
            }

            asort($persons);

            $return[$objArchive->title] = $persons;
        }

        return $return;

    }
}

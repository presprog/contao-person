<?php

/**
 * Namespace
 */
namespace Contao;

/**
 * Class PersonEntryModel
 *
 * @copyright  mindbird 2013
 * @author     mindbird
 * @package    Devtools
 */
class PersonModel extends \Model
{

    /**
     * Name of the table
     * @var string
     */
    protected static $strTable = 'tl_person';

    public function getFullName($inverted = false) {
        if ($inverted) {
            return $this->lastname . ', ' . $this->firstname;
        }
        return $this->firstname . ' ' . $this->lastname;
    }

}

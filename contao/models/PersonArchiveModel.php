<?php

/**
 * Namespace
 */
namespace Contao;

/**
 * Class PersonArchiveModel
 *
 * @copyright  mindbird 2013
 * @author     mindbird
 * @package    Devtools
 */
class PersonArchiveModel extends \Model
{

    /**
     * Name of the table
     * @var string
     */
    protected static $strTable = 'tl_person_archive';

}
